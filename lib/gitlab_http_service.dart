import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:shared_preferences/shared_preferences.dart';

class GitlabHttpService {
  static const String InstancePrefKey = 'gitlab-instance';
  static const String AccessTokenPrefKey = 'gitlab-access-token';
  static const String UserJsonKey = 'gitlab-user';

  dynamic loggedInUser = {'username': ''};

  Future<dynamic> get user {
    if(loggedInUser != null) {
      return new Future.sync(() => loggedInUser);
    }
    return SharedPreferences.getInstance().then((prefs) {
      var userJson = prefs.get(UserJsonKey);
      loggedInUser = JSON.decode(userJson);
      return loggedInUser;
    });
  }

  Future<dynamic> login() async {
    loggedInUser = await doGet('user', {'visibility': 'private'});
    var prefs = await SharedPreferences.getInstance();
    prefs.setString(UserJsonKey, JSON.encode(loggedInUser));
    return loggedInUser;
  }

  Future<dynamic> doGet(String endpoint, Map<String, String> params) async {
    var httpClient = new HttpClient();
    var credentials = await loadPreferences();
    var uri = new Uri.https(
        credentials.instanceUrl,
        '/api/v4/$endpoint',
        {'private_token': credentials.accessToken, 'visibility': 'private'}
          ..addAll(params));
    print('Making request to $uri');
    var request = await httpClient.getUrl(uri);
    var response = await request.close();
    var body = await response.transform(UTF8.decoder).join();
    print('Got response: $body');
    if (response.statusCode != 200) {
      return null;
    }
    return JSON.decode(body);
  }

  Future<Null> saveConnectPreferences(
      String instance, String accessToken) async {
    var _prefs = await SharedPreferences.getInstance();
    _prefs.setString(InstancePrefKey, instance);
    _prefs.setString(AccessTokenPrefKey, accessToken);
  }

  Future<Credentials> loadPreferences() async {
    var _prefs = await SharedPreferences.getInstance();
    String instance = _prefs.getString(InstancePrefKey);
    String accessToken = _prefs.getString(AccessTokenPrefKey);
    return new Credentials(instance, accessToken);
  }
}

class Credentials {
  final String instanceUrl;
  final String accessToken;

  const Credentials(this.instanceUrl, this.accessToken);
}
