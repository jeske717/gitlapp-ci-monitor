import 'package:flutter/material.dart';
import 'package:gitlapp_ci_monitor/gitlab_http_service.dart';

class SettingsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  final GitlabHttpService _httpService = new GitlabHttpService();

  Iterable<Group> _groups = [];

  @override
  void initState() {
    super.initState();
    _httpService.user
        .then((user) => _httpService.doGet('groups', {}))
        .then((groups) {
      setState(() {
        _groups = groups.map((g) => new Group(g['id'], g['name']));
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Settings'),
      ),
      body: new ListView(
        padding: const EdgeInsets.all(16.0),
        children: ListTile
            .divideTiles(
              context: context,
              tiles: _createListChildren(context),
            )
            .toList(),
      ),
    );
  }

  List<Widget> _createListChildren(BuildContext context) {
    List<Widget> result = [];
    result.add(new ListTile(
      title: new Text(_httpService.loggedInUser['username']),
      trailing: new Icon(Icons.chevron_right),
      onTap: () {
        _showUserProjects(context);
      },
    ));
    result.addAll(_groups.map((g) {
      return _createListRow(context, g);
    }));
    return result;
  }

  Widget _createListRow(BuildContext context, Group g) {
    return new ListTile(
      title: new Text(g.name),
      trailing: new Icon(Icons.chevron_right),
      onTap: () {
        _showGroup(context, g);
      },
    );
  }

  _showUserProjects(BuildContext context) {
    Navigator.of(context).pushNamed('/settings/user-projects/${_httpService.loggedInUser['id']}');
  }

  _showGroup(BuildContext context, Group group) {
    Navigator.of(context).pushNamed('/settings/group/${group.id}');
  }
}

class Group {
  final int id;
  final String name;

  Group(this.id, this.name);
}
