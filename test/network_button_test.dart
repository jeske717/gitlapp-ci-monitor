// This is a basic Flutter widget test.
// To perform an interaction with a widget in your test, use the WidgetTester utility that Flutter
// provides. For example, you can send tap and scroll gestures. You can also use WidgetTester to
// find child widgets in the widget tree, read text, and verify that the values of widget properties
// are correct.

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:gitlapp_ci_monitor/network_button.dart';

void main() {
  testWidgets(
      'renders a progress indicator when tapped until the future completes',
      (WidgetTester tester) async {

    var buttonKey = new UniqueKey();
    var textChildKey = new UniqueKey();

    var networkCall = new Completer();
    await tester.pumpWidget(new MaterialApp(
      home: new Material(
        child: new Center(
          child: new NetworkButtonWidget(
            key: buttonKey,
            onPressed: () => networkCall.future,
            child: new Text('foo', key: textChildKey),
          ),
        ),
      ),
    ));

    await tester.tap(find.byKey(buttonKey));
    await tester.pump();
    expect(find.byKey(textChildKey), findsNothing);
    expect(find.byType(CircularProgressIndicator), findsOneWidget);

    networkCall.complete(new Object());

    await tester.pump();
    expect(find.byKey(textChildKey), findsOneWidget);
    expect(find.byType(CircularProgressIndicator), findsNothing);
  });
}
