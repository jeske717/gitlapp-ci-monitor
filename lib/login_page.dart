import 'dart:async';

import 'package:flutter/material.dart';
import 'package:gitlapp_ci_monitor/gitlab_http_service.dart';
import 'package:gitlapp_ci_monitor/network_button.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _apiTokenController = new TextEditingController();
  final _instanceController = new TextEditingController(text: 'gitlab.com');
  final _httpService = new GitlabHttpService();

  @override
  void initState() {
    super.initState();
    _asyncInitState();
  }

  _asyncInitState() async {
    var credentials = await _httpService.loadPreferences();
    _instanceController.text = credentials.instanceUrl;
    _apiTokenController.text = credentials.accessToken;
    if(credentials.instanceUrl != null && credentials.accessToken != null) {
      await _httpService.login();
      setState(() => {});
    }
  }

  Future _doLoginFlow(BuildContext context) async {
    var user = await _httpService.user;
    Scaffold.of(context).showSnackBar(
        new SnackBar(content: new Text('Welcome, ${user['name']}')));
    await Navigator.of(context).pushReplacementNamed('/home');
  }

  Future<Null> _submitPressed(BuildContext context) async {
    await _httpService.saveConnectPreferences(
        _instanceController.text, _apiTokenController.text);
    await _httpService.login();
    setState(() => {});
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Login'),
      ),
      body: new Builder(builder: (BuildContext context) {
        if (_httpService.loggedInUser != null) {
          _doLoginFlow(context);
        }
        return new Center(
          child: new Container(
            padding: const EdgeInsets.all(16.0),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new TextField(
                  controller: _apiTokenController,
                  decoration:
                  new InputDecoration(hintText: 'Gitlab API Access Token'),
                ),
                new TextField(
                  controller: _instanceController,
                  decoration: new InputDecoration(
                      hintText: 'Gitlab instance URL (assumes HTTPS)'),
                ),
                new Padding(
                  padding: const EdgeInsets.only(top: 16.0),
                  child: new NetworkButtonWidget(
                    onPressed: () {
                      return _submitPressed(context);
                    },
                    child: new Text('Login'),
                  ),
                ),
              ],
            ),
          ),
        );
      }),
    );
  }
}
