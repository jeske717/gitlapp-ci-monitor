import 'dart:async';

import 'package:flutter/material.dart';
import 'package:gitlapp_ci_monitor/constants.dart';
import 'package:gitlapp_ci_monitor/gitlab_http_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _httpService = new GitlabHttpService();
  List<_BuildStatus> _statuses = [];
  bool _loading = false;
  Map<String, Color> _statusColors = {
    'success': Colors.green,
    'running': Colors.blue,
    'pending': Colors.blueGrey,
    'failed': Colors.red,
    'canceled': Colors.grey,
    'skipped': Colors.grey
  };

  @override
  void initState() {
    super.initState();
    _asyncInit();
  }

  _asyncInit() async {
    setState(() {
      _loading = true;
    });
    var s = await _loadStatuses();
    setState(() {
      _statuses = s;
      _loading = false;
    });
  }

  Future<List<_BuildStatus>> _loadStatuses() async {
    var prefs = await SharedPreferences.getInstance();
    var projectIds =
        (prefs.getStringList(SavedProjectsKey) ?? []).map((s) => int.parse(s));

    List<_BuildStatus> newStatuses = new List<_BuildStatus>();
    for (int id in projectIds) {
      List<dynamic> branches =
          await _httpService.doGet('projects/$id/repository/branches', {});
      for (dynamic b in branches) {
        String branchName = b['name'];
        List<dynamic> pipelines = await _httpService.doGet(
            'projects/$id/pipelines', {'ref': branchName, 'per_page': '1'});
        if (pipelines.isNotEmpty) {
          var pipeline = pipelines[0];
          newStatuses.add(new _BuildStatus(
              prefs.getString('$ProjectNameKey$id'),
              branchName,
              pipeline['status']));
        }
      }
    }
    return newStatuses;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Gitlab CI Monitor'),
        actions: <Widget>[
          new IconButton(
            icon: new Icon(Icons.settings),
            onPressed: () {
              _settingsClicked(context);
            },
          )
        ],
      ),
      body: new RefreshIndicator(
              onRefresh: _asyncInit,
              child: new ListView(
                children: _listViewChildren(context),
              ),
            ),
    );
  }

  List<Widget> _listViewChildren(BuildContext context) {
    List<Widget> result = [];
    if (_loading) {
      result.add(new LinearProgressIndicator());
    }
    result.addAll(ListTile
        .divideTiles(
            context: context,
            color: Colors.black,
            tiles: _statuses.map((s) {
              return _createStatusRow(s);
            }))
        .toList());
    return result;
  }

  Widget _createStatusRow(_BuildStatus status) {
    return new Container(
      color: _statusColors[status.status],
      child: new Padding(
        padding: const EdgeInsets.all(8.0),
        child: new ListTile(
          title: new Text('${status.projectName} (${status.branchName})'),
          subtitle: new Text(status.status),
        ),
      ),
    );
  }

  _settingsClicked(BuildContext context) {
    Navigator.of(context).pushNamed('/settings').then((_) => _asyncInit());
  }
}

class _BuildStatus {
  final String projectName;
  final String branchName;
  final String status;

  _BuildStatus(this.projectName, this.branchName, this.status);
}
