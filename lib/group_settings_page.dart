import 'package:flutter/material.dart';
import 'package:gitlapp_ci_monitor/constants.dart';
import 'package:gitlapp_ci_monitor/gitlab_http_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

typedef String EndpointBuilder(int);

class GroupSettingsPage extends StatefulWidget {
  final int _groupId;
  final EndpointBuilder _endpointBuilder;

  GroupSettingsPage(this._groupId, this._endpointBuilder);

  @override
  State<StatefulWidget> createState() => new _GroupSettingsPageState();
}

class _GroupSettingsPageState extends State<GroupSettingsPage> {
  final _httpService = new GitlabHttpService();
  Iterable<Project> _projects = [];
  Set<String> _savedProjectIds;

  @override
  void initState() {
    super.initState();
    _asyncInit();
  }

  _asyncInit() async {
    var prefs = await SharedPreferences.getInstance();
    _savedProjectIds = new Set.from(prefs.getStringList(SavedProjectsKey) ?? []);
    List<dynamic> groupProjects = await _httpService.doGet(widget._endpointBuilder(widget._groupId), {});
    setState(() {
      _projects = groupProjects.map((p) => new Project(p['id'], p['name'], _savedProjectIds.contains(p['id'].toString())));
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Select projects to monitor'),
      ),
      body: new ListView(
        padding: const EdgeInsets.all(16.0),
        children: ListTile.divideTiles(
          context: context,
          tiles: _projects.map((p) { return _createListRow(p); } )
        ).toList()
      ),
    );
  }

  _createListRow(Project project) {
    return new ListTile(
      title: new Text(project.name),
      trailing: new Switch(
        value: project.checked,
        onChanged: (b) {
          if(b) {
            _savedProjectIds.add(project.id.toString());
          } else {
            _savedProjectIds.remove(project.id.toString());
          }
          _updatePreferences(project);
          setState(() {
            project.checked = b;
          });
        },
      ),
    );
  }

  _updatePreferences(Project project) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setStringList(SavedProjectsKey, _savedProjectIds.toList());
    prefs.setString('$ProjectNameKey${project.id}', project.name);
  }
}

class Project {
  final int id;
  final String name;
  bool checked = false;

  Project(this.id, this.name, this.checked);
}
