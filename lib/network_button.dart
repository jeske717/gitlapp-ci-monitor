import 'dart:async';

import 'package:flutter/material.dart';

typedef Future NetworkCall();

class NetworkButtonWidget extends StatefulWidget {
  const NetworkButtonWidget({
    Key key,
    this.onPressed,
    this.child,
  })
      : super(key: key);

  final NetworkCall onPressed;
  final Widget child;

  @override
  State<StatefulWidget> createState() => new _NetworkButtonWidgetState();
}

class _NetworkButtonWidgetState extends State<NetworkButtonWidget> {
  bool _inProgress = false;

  @override
  Widget build(BuildContext context) {
    if (_inProgress) {
      return new CircularProgressIndicator();
    }
    return new RaisedButton(
      onPressed: () {
        setState(() {
          _inProgress = true;
        });
        widget.onPressed().whenComplete(() {
          setState(() {
            _inProgress = false;
          });
        });
      },
      child: widget.child,
    );
  }
}
