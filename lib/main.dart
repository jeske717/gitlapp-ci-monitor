import 'package:flutter/material.dart';
import 'package:gitlapp_ci_monitor/group_settings_page.dart';
import 'package:gitlapp_ci_monitor/home_page.dart';
import 'package:gitlapp_ci_monitor/login_page.dart';
import 'package:gitlapp_ci_monitor/settings_page.dart';

void main() => runApp(new GitlappCiMonitorApp());

class GitlappCiMonitorApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Gitlapp CI Monitor',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new LoginPage(),
      routes: <String, WidgetBuilder>{
        '/home': (context) => new HomePage(),
        '/settings': (context) => new SettingsPage(),
      },
      onGenerateRoute: (settings) {
        var parts = settings.name.split('/');
        if (parts.length == 4 &&
            parts[1] == 'settings' &&
            parts[2] == 'group') {
          return new MaterialPageRoute(
              builder: (BuildContext context) =>
                  new GroupSettingsPage(int.parse(parts[3]), (id) => 'groups/$id/projects'));
        }
        if (parts.length == 4 &&
            parts[1] == 'settings' &&
            parts[2] == 'user-projects') {
          return new MaterialPageRoute(
              builder: (BuildContext context) =>
              new GroupSettingsPage(int.parse(parts[3]), (_) => 'projects'));
        }
      },
    );
  }
}
